using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Roll
{

    public class Roll : MonoBehaviour
    {
        public int diceType;
        public int end_roll;

        public static int RollDice(int diceType)
        {
            if (diceType == 20)
            {
                return Random.Range(1, 21);
            }
            if (diceType == 12)
            {
                return Random.Range(1, 13);
            }
            if (diceType == 10)
            {
                return Random.Range(1, 11);
            }
            if (diceType == 8)
            {
                return Random.Range(1, 9);
            }
            if (diceType == 6)
            {
                return Random.Range(1, 7);
            }
            if (diceType == 4)
            {
                return Random.Range(1, 5);
            }
            else
                return 0;
        }
    }
}

            
      
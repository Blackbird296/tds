using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform followTransform;
    public BoxCollider2D mapbounds;

    private float xMin, xMax, yMin, yMax;
    private float camY, camX;
    private float camOrthsize;
    private float cameraRatio;
    private Camera MainCamera;
    // Start is called before the first frame update
    private void Start()
    {
        xMin = mapbounds.bounds.min.x;
        xMax = mapbounds.bounds.max.x;
        yMin = mapbounds.bounds.min.y;
        yMax = mapbounds.bounds.max.y;
        MainCamera = GetComponent<Camera>();
        camOrthsize = MainCamera.orthographicSize;
        cameraRatio = (xMax + camOrthsize) / 2.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        camY = Mathf.Clamp(followTransform.position.y, yMin + camOrthsize, yMax - camOrthsize);
        camX = Mathf.Clamp(followTransform.position.x, xMin + cameraRatio, xMax - cameraRatio);
        this.transform.position = new Vector3(camX, camY, this.transform.position.z);
    }
}

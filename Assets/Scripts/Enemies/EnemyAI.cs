using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class EnemyAI : MonoBehaviour, IDamageable
    {
    public float Health { get; set; }
        // Start is called before the first frame update
        void Start()
        {
         //   Health = _health;
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void Damage()
        {
        Health--;
        if (Health == 0)
        {
            Destroy(this.gameObject);
        }
    }
    }

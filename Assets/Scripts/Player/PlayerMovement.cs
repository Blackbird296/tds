using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float startSpeed = 10f; // Setting Velocity Modifier
    [SerializeField] private float sensativity = 60f;
    private float speed;
    public int stealthMod;
    public bool isCrouched = false;
    public bool isSprinting = false;
    // Start is called before the first frame update
    void Start()
    {
        stealth = player.GetComponent<Stealth>();
        rb = GetComponent<Rigidbody2D>();
        speed = startSpeed + dexMod;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 objectScale = transform.localScale;
        float yInt = Input.GetAxisRaw("Horizontal");
        float xInt = Input.GetAxisRaw("Vertical");
        float zRot = Input.GetAxisRaw("Rotation"); // Q key Changes value to -1, E Key changes Value to 1
        rb.angularVelocity = zRot * sensativity;
        rb.velocity = ((transform.up * speed) * xInt);
        if (yInt == 1f)
        {
            rb.AddForce(transform.right * speed);
        }
        if (yInt == -1f)
        {
            rb.AddForce(-transform.right * speed);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl)) // Crouching
        {
            Crouch();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)) // Sprint
        {
            Sprint();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Sprint();
        }
    }
    void Sprint()
    {
        if (isSprinting == false)
        {
            SprintStart();
        }
        else
        {
            SprintEnd();
        }
        void SprintStart()
        {
            Debug.Log("Sprinting");
            isSprinting = true;
            speed = (speed * 2f);
        }
        void SprintEnd()
        {
            isSprinting = false;
            speed = (speed / 2f);
        }

    }
    void Crouch()
    {
        if (isCrouched == true)
        {
            isCrouched = false;
            Debug.Log("Standing");
            speed = speed * 2f;
            stealth.exitStealth();
        }
        else
        {
            isCrouched = true;
            speed = speed * .5f;
            stealth.StartStealth();
        }
    }
}
*/
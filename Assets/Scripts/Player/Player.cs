using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Magic;
using Roll;

public class Player : MonoBehaviour
{
 

    /// <Summary>
    /// This Class contains all Player Core and Functions please dont touch unless Bird or you know what your doing cause i sure as hell dont
    /// 
    /// 
    /// 
    /// 
    /// 
    /// 
    /// </summary>
    // Define Stats
    [Header("Stats")]
    public int strength = 10; // How much you can lift
    public int dexterity = 10; // How speedy or dexterous you are
    public int constitution = 10;  // Health
    public int intelligence = 10; // Intelligence
    public int perception = 10; // How much you can see 
    public int Arcane = 10; // Basic Understanding of Magic
    [HideInInspector] public int strMod;
    [HideInInspector] public int dexMod;
    [HideInInspector] public int conMod;
    [HideInInspector] public int intMod;
    [HideInInspector] public int perMod;
    [HideInInspector] public int ArcMod;
    // Define Movement
    [Header("Movement")]
    [SerializeField] private float startSpeed = 10f; // Setting Velocity Modifier
    [SerializeField] private float sensativity = 60f;
    private float speed;
    public int stealthMod;
    public bool isCrouched = false;
    public bool isSprinting = false;
    // Define Combat
    [Header("Combat")]
    public int Health = 100;
    private int HealthMax;
    public int ArmorClass;
    public float PlayerRng;
    public float Playerdmg;
    public float PlayermDmg;
    public int maxAmmo;
    private int ammo;
    private bool isJammed;
    // Define Sight
    [Header("Sight")]
    public int Perc_roll;
    public int baseRoll;
    public int closeRoll;
    public int medRoll;
    public int farRoll;
    [Header("Magic")]
    private bool magicStart;
    public int mana;
    public string manaType;
    public bool isCaster;
    public int manaMax;

    // Define References
    [Header("References")]
    public GameObject player;
    public Rigidbody2D rb;
    public Stealth stealth;
    public WeaponHandler wh;
    public GameObject firingPoint;
    public GameObject closeRange;
    public GameObject medRange;
    public GameObject farRange;
    // Interact
    // Start is called before the first frame update
    void Start()
    {
        StatMods();
        AbilityMods();
        stealth = player.GetComponent<Stealth>();
        rb = GetComponent<Rigidbody2D>();
        speed = startSpeed + dexMod;
        Health = HealthMax;
        ammo = maxAmmo;
        if (!magicStart)
        {
            if (isCaster)
            {
                manaMax = Arcane * 5;
                manaType = "Neutral";
                mana = manaMax;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 objectScale = transform.localScale;
        float yInt = Input.GetAxisRaw("Horizontal");
        float xInt = Input.GetAxisRaw("Vertical");
        float zRot = Input.GetAxisRaw("Rotation"); // Q key Changes value to -1, E Key changes Value to 1
        rb.angularVelocity = zRot * sensativity;
        rb.velocity = ((transform.up * speed) * xInt);
        if (yInt == 1f)
        {
            rb.AddForce(transform.right * speed);
        }
        if (yInt == -1f)
        {
            rb.AddForce(-transform.right * speed);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl)) // Crouching
        {
            Crouch();
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)) // Sprint
        {
            Sprint();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Sprint();
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            Debug.Log("Active Sight Activating");
            Perc_roll = Roll.Roll.RollDice(20);
            Debug.Log("Your Perception roll is " + Perc_roll);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            RaycastHit2D interact = Physics2D.Raycast(firingPoint.transform.position, Vector2.up);
            Debug.DrawRay(firingPoint.transform.position, Vector2.up, Color.blue);
            if (interact.transform.tag == "Interactable")
            {
                InteractWithObject(interact.collider.gameObject);
                Debug.Log("Interactable name is " + interact.transform.name);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(firingPoint.transform.position, Vector2.up);
            if (hit.transform.tag == "Enemy")
            {
                Debug.DrawRay(firingPoint.transform.position, Vector2.up, Color.red);
                IDamageable target = hit.transform.GetComponent<IDamageable>();

                if (target != null)
                {
                    target.Damage();
                    Debug.Log("health: " + target.Health);
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {

        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }
    void StatMods()
        {
            strMod = strength - 10 / 2 + 1;
            dexMod = dexterity - 10 / 2 + 1;
            conMod = constitution - 10 / 2 + 1;
            intMod = intelligence - 10 / 2 + 1;
            perMod = perception - 10 / 2 + 1;
            ArcMod = Arcane - 10 / 2 + 1;
        }
    void AbilityMods()
        {

        }
        void Reload()
    {
        ammo = maxAmmo;
    }

    void SightUpdate()
    {
        // Passive Sight
        
        // Active Sight
        if (Input.GetKeyDown(KeyCode.V))
        {
            Debug.Log("Active Sight Activating");
            SightActive();
            Debug.Log("Your Perception roll is" + Perc_roll);
        }
        void SightActive()
        {
            Perc_roll = Roll.Roll.RollDice(20);
        }

    }

    void MovementUpdate()
    {
       
    }
    void InteractWithObject(GameObject objectToInteractWith)
    {
        if (objectToInteractWith.TryGetComponent(out IClickable clickableObject))
        {
            clickableObject.Interact();
        }

    }
    void Sprint()
    {
        if (isSprinting == false)
        {
            SprintStart();
        }
        else
        {
            SprintEnd();
        }
        void SprintStart()
        {
            Debug.Log("Sprinting");
            isSprinting = true;
            speed = (speed * 2f);
        }
        void SprintEnd()
        {
            isSprinting = false;
            speed = (speed / 2f);
        }

    }
    void Crouch()
    {
        if (isCrouched == true)
        {
            isCrouched = false;
            Debug.Log("Standing");
            speed = speed * 2f;
            stealth.exitStealth();
        }
        else
        {
            isCrouched = true;
            speed = speed * .5f;
            stealth.StartStealth();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public float PlayerRng;
    public float Playerdmg;
    public float PlayermDmg;
    private int maxAmmo;
    private int ammo;
    private bool isJammed;

    private GameObject weapon;

    // Start is called before the first frame update
    void Start()
    {
        maxAmmo = 12;
        ammo = maxAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(ammo>0)
            {
                Fire();
            }
            if(ammo == 0)
            {
                Debug.Log("reload");
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Aim();
        }   
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }
    void Fire()
    {
        Physics2D.Raycast(transform.position, transform.forward);
        ammo--;
    }
    void Aim()
    {
        // Look at mouse

    }
    void Reload()
    {

        ammo = maxAmmo;

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IClickable
{
    public float openAngle = -90f;

    // Angle at which the door is fully closed
    public float closedAngle = 0f;
    public GameObject door;

    HingeJoint2D hinge;
    public bool isOpen = false;
    // Start is called before the first frame update
    void Start()
    {
        hinge = door.GetComponent<HingeJoint2D>();
        Close();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Interact()
    {
        Debug.Log(this.transform.name + " has been interacted with");
        // Door Toggled
        if (isOpen ==false)
        {
            Open();
        }
        else
        {
            Close();
        }
    }
    void Open()
    {
        Debug.Log("opening");
       hinge.attachedRigidbody.AddForce(Vector2.up);


    }
    void Close()
    {
        Debug.Log("closing");
       
    }
}
